
$(document).ready(function() {

   $('#goods-table').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "/goods/list",

        "columns": [
            { "data": "good_id",
                "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).html('<div class="checkbox"><label><input type="checkbox">' +
                        cellData + '</label></div>');
                }
            },
            { "data": "good_name",
                "createdCell": function (td, cellData, rowData, row, col) {
                    var url = "/good/" + rowData.good_id;
                    $(td).html('<a href="'+ url +'">' + cellData + '</a><br>Внешний ID: ' + rowData.good_external_id);
                }
            },
            { "data": "good_price" },
            { "data": "advert",
                "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).html('<a href="javascript:void(0)">' + cellData.user_first_name + " " +
                        cellData.user_last_name + '</a><br>' + cellData.email);
                }

            }
        ],
        "pagingType": "full_numbers",
        "oLanguage": { //
            "sLengthMenu": '<div class="form-group-dt">Показывать по <select class="form-data-table">'+
            '<option value="1">1</option>'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="20">20</option>'+
            '</select> на странице </div> <div class="clearfix"></div>',
            "sSearch": "Поиск"

        }
    } );


} );