
var ordersTable = null;
var onSubmit = false;
$(document).ready(function() {

    ordersTable = $('#orders-table').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/orders/list",
            "data":  function ( d ) {
                if(onSubmit){
                    d["advanced_filter"] = {
                        "date_from":  $('#date-from').val(),
                        "date_to":  $('#date-to').val(),
                        "oder_state":  $('#oder-state').val(),
                        "order_phone":  $('#order-phone').val(),
                        "order_good":  $('#order-good').val(),
                        "order_id":  $('#order-id').val()
                    };
                    onSubmit = false;
                }
            }
        },
        "columns": [
            { "data": "order_id" },
            { "data": "order_add_time_formatted" },
            { "data": "user_client_name" },
            { "data": "user_client_phone" },
            { "data": "good",
              "createdCell": function (td, cellData, rowData, row, col) {
                  var url = "/good/" + cellData.good_id;
                  var user = cellData.advert.user_first_name + " " +
                          cellData.advert.user_last_name + " (" + cellData.advert.email +")";
                     $(td).html('<a href="'+ url +'">' + cellData.good_name + '</a><br>' + user);
                }

            },
            { "data": "state.state_name" }
        ],
        "pagingType": "full_numbers",
        "oLanguage": { //
            "sLengthMenu": '<div class="form-group-dt">Показывать по <select class="form-data-table">'+
            '<option value="1">1</option>'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="20">20</option>'+
            '</select> на странице </div> <div class="clearfix"></div>',
            "sSearch": "Поиск"

        }
    } );

    $('#filter-submit').on("click",
        function(){
            onSubmit = true;
            ordersTable.draw();
        });

} );