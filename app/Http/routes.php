<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', [
    "as" => "user_login",
    "uses" => "Auth\\AuthController@getLogin"
]);
Route::post('/', [
    "uses" => "Auth\\AuthController@postLogin"
]);

Route::get('/logout', [
    "as" => "user_logout",
    "uses" => "Auth\\AuthController@getLogout"
]);

Route::group(['middleware' => 'auth'], function () {
    Route::get('/orders', [
        "as" => "orders",
        "uses" => "Backend\\OrderController@getIndex"
    ]);

    Route::get('/orders/list', [
        "as" => "orders.list",
        "uses" => "Backend\\OrderController@getList"
    ]);

    Route::get('/goods', [
        "as" => "goods",
        "uses" => "Backend\\GoodController@getIndex"
    ]);

    Route::get('/goods/list', [
        "as" => "goods.list",
        "uses" => "Backend\\GoodController@getList"
    ]);

    Route::get('/good/{good}', [
        "as" => "good.show",
        "uses" => "Backend\\GoodController@getGood"
    ]);
    Route::post('/good/{good}', [
        "uses" => "Backend\\GoodController@postGood"
    ]);


});