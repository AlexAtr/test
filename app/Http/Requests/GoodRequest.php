<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class GoodRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'good_external_id' => "integer",
            "good_advert"      => "required|integer|exists:adverts,user_id",
            "good_name"        => "required|max:255",
            "good_price"        => "required|numeric|min:0",
        ];
    }
}
