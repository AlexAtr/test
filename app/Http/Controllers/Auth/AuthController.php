<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Models\Advert;
use Faker\Provider\Base;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'user_first_name' => 'required|max:255',
            'user_last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:adverts',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Advert
     */
    protected function create(array $data)
    {
        return Advert::create([
            'user_first_name' => $data['user_first_name'],
            'user_last_name'  => $data['user_last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


    public function getLogin(Guard $auth){

        if($auth->check()){
            return redirect()->route('orders');
        }
        $this->title ="Login Page";

        return $this->render(__FUNCTION__);
    }


    public function postLogin(Request $request,  Guard $auth){
        if ($auth->attempt($request->only(['email','password']), true)){
            return redirect()->route('orders');
        }

        $error = new \Illuminate\Support\MessageBag;
        $error->merge(["Invalid email/password pair"]);
        return redirect()->back()->withInput($request->only("email"))->with("errors", $error);
    }


    public function getLogout(Guard $auth){
        $auth->logout();
        return redirect()->route("user_login");
    }
}
