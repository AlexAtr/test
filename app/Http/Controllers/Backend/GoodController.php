<?php
namespace App\Http\Controllers\Backend;


use App\Http\Controllers\BaseController;
use App\Http\Requests\GoodRequest;
use App\Models\Advert;
use App\Models\Good;
use Illuminate\Http\Request;

class GoodController extends BaseController
{

    public function __construct(){
        $this->activeMenu = "goods";
    }

    public function getIndex(){
        $this->title = "Товары";
        return $this->render(__FUNCTION__);
    }


    public function getList(Request $request){
        $input = $request->all();

        $query = Good::with(['advert']);
        $totalRows = $query->count();

        $query->join('adverts', 'adverts.user_id', '=', 'goods.good_advert');

        if(!empty($input["search"]["value"])){
            $filter = $input["search"]["value"];
            $query->where(function($qry) use($filter){
                $qry->orWhere("goods.good_id", "like", "%".$filter."%")
                    ->orWhere("goods.good_name", "like", "%".$filter."%")
                    ->orWhere("goods.good_external_id", "like", "%".$filter."%")
                    ->orWhere("goods.good_price", "=", $filter)
                    ->orWhere("adverts.user_first_name", "like", "%".$filter."%")
                    ->orWhere("adverts.user_last_name", "like", "%".$filter."%")
                    ->orWhere("adverts.email", "like", "%".$filter."%");
            });
        }

        $filteredRows = $query->distinct()->count(["goods.good_id"]);

        $orderBy = $request->input('order.0.column', '0');
        $dir = $request->input('order.0.dir', 'asc');

        switch ($orderBy) {
            case '0':
                $query->orderBy('goods.good_id', $dir);
                break;
            case '1':
                $query->orderBy('goods.good_name', $dir);
                break;
            case '2':
                $query->orderBy('goods.good_price', $dir);
                break;
            case '3':
                $query->orderBy('goods.user_first_name', $dir)
                      ->orderBy('goods.user_last_name', $dir);
                break;


        }

        $goods = $query->skip($input["start"])->take($input["length"])->select("goods.*")->get();

        return array(
            "draw"            => $request->input('draw', 0),
            "recordsTotal"    => $totalRows,
            "recordsFiltered" => $filteredRows,
            "data"            => $goods
        );
    }


    public function getGood(Request $request, Good $good){
        $this->title = "Данные товара";
        $this->viewData["good"] = $good;
        $adverts = Advert::all();
        $this->viewData["adverts"] = $adverts->pluck("info_text", "user_id");
        return $this->render(__FUNCTION__);
    }


    public function postGood(GoodRequest $request, Good $good){
        $good->update($request->only(["good_external_id", "good_name", "good_price", "good_advert"]));
        $msg = new \Illuminate\Support\MessageBag;
        $msg->merge(["Данные были обновлены."]);
        return redirect()->back()->with('success', $msg);
    }
}