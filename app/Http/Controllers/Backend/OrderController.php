<?php

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\BaseController;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends BaseController
{


    public function __construct(){
        $this->activeMenu = "orders";
    }

    public function getIndex(){
        $this->title = "Заказы";
        return $this->render(__FUNCTION__);
    }

    public function getList(Request $request){
        $input = $request->all();

        $query = Order::with(['state', 'good.advert']);
        $totalRows = $query->count();

        $query->join('goods', 'goods.good_id', '=', 'orders.order_good')
            ->join('states', 'states.state_id', '=', 'orders.order_state')
            ->join('adverts', 'adverts.user_id', '=', 'goods.good_advert');

        if(!empty($input["advanced_filter"])){
            if(!empty($input["advanced_filter"]["date_from"])){
                $dt = Carbon::createFromFormat("d.m.y", $input["advanced_filter"]["date_from"]);
                if($dt && $dt->format("d.m.y") == $input["advanced_filter"]["date_from"])
                    $query->where("orders.order_add_time", ">=", $dt);
            }
            if(!empty($input["advanced_filter"]["date_to"])){
                $dt = Carbon::createFromFormat("d.m.y", $input["advanced_filter"]["date_to"]);
                if($dt && $dt->format("d.m.y") == $input["advanced_filter"]["date_to"])
                    $query->where("orders.order_add_time", "<=", $dt);
            }
            if(!empty($input["advanced_filter"]["oder_state"])){
                $query->where("states.state_name",
                    "like", "%". $input["advanced_filter"]["oder_state"] . "%");
            }
            if(!empty($input["advanced_filter"]["order_phone"])){
                $query->where("orders.user_client_phone",
                    "like", "%". $input["advanced_filter"]["order_phone"] . "%");
            }
            if(!empty($input["advanced_filter"]["order_good"])){
                $query->where("goods.good_name",
                    "like", "%". $input["advanced_filter"]["order_good"] . "%");
            }
            if(!empty($input["advanced_filter"]["order_id"])){
                $query->where("orders.order_id",
                    "like", "%". $input["advanced_filter"]["order_id"] . "%");
            }

        }else if(!empty($input["search"]["value"])){
            $filter = $input["search"]["value"];
            $query->where(function($qry) use($filter){
                $qry->orWhere("orders.order_id", "like", "%".$filter."%")
                    ->orWhere("orders.user_client_name", "like", "%".$filter."%")
                    ->orWhere("orders.user_client_phone", "like", "%".$filter."%")
                    ->orWhere("goods.good_name", "like", "%".$filter."%")
                    ->orWhere("adverts.email", "like", "%".$filter."%")
                    ->orWhere("adverts.user_first_name", "like", "%".$filter."%")
                    ->orWhere("adverts.user_last_name", "like", "%".$filter."%")
                    ->orWhere("states.state_name", "like", "%".$filter."%");
            });
        }

        $filteredRows = $query->distinct()->count(["orders.order_id"]);

        $orderBy = $request->input('order.0.column', '0');
        $dir = $request->input('order.0.dir', 'asc');

        switch ($orderBy) {
            case '0':
                $query->orderBy('orders.order_id', $dir);
                break;
            case '1':
                $query->orderBy('orders.order_add_time', $dir);
                break;
            case '2':
                $query->orderBy('orders.user_client_name', $dir);
                break;
            case '3':
                $query->orderBy('orders.user_client_phone', $dir);
                break;
            case '4':
                $query->orderBy('goods.good_name', $dir);
                break;
            case '5':
                $query->orderBy('states.state_name', $dir);
                break;

        }

        $orders = $query->skip($input["start"])->take($input["length"])->select("orders.*")->get();

        return array(
            "draw"            => $request->input('draw', 0),
            "recordsTotal"    => $totalRows,
            "recordsFiltered" => $filteredRows,
            "data"            => $orders
        );
    }

}