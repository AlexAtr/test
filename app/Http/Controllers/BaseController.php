<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;

class BaseController extends Controller
{
    protected $viewData = [];
    protected $title;
    protected $description;
    protected $keywords;
    protected $bodyClass;
    protected $bodyId;

    protected $viewPrefix = '';
    protected $beforeRender = [];
    protected $activeMenu = '';



    protected function render($view, array $data = []){
        return $this->renderView($view, $data);
    }


    protected function renderView($view, array $data = []){
        foreach($this->beforeRender as $method) $this->$method();

        $this->viewData['_siteTitle'] = $this->title;
        $this->viewData['_siteDescription'] = $this->description;
        $this->viewData['_siteKeywords'] = $this->keywords;
        $this->viewData['_activeMenu'] = $this->activeMenu;

        $strClass = get_class($this);
        $className = explode('\\', strstr($strClass, "Controllers"));
        $newClassName = "";
        $classViewPrefix = "";
        foreach ((array)$className as $key => $name) {
            if($key+1 === count($className) ){
                $newClassName .= strtolower(str_replace("Controller", "", end($className)));
                $classViewPrefix .= end($className);
            }else if($key === count($className) - 2){
                $newClassName .= strtolower($name) ."-";
                $classViewPrefix .= $name. ".";
            }
        }

        $this->viewData['_bodyClass'] = $newClassName . ' '. $this->bodyClass;
        $this->viewData['_bodyId'] = $this->bodyId;

        if($this->viewPrefix){
            $nbView = $this->viewPrefix.'.'.$view;
        }else{
            $nbView = "controllers." . $classViewPrefix . "." . $view;
        }

        if(view()->exists($nbView)){
            $view = $nbView;
        }

        return response()->view($view, array_merge($this->viewData, $data));
    }
}
