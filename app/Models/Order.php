<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $primaryKey = "order_id";

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'order_id', 'created_at', 'updated_at'
    ];


    protected $dates = ['updated_at', 'created_at', 'order_add_time'];

  //  protected $dateFormat = 'd.m.y H:i';

    protected $appends = ["order_add_time_formatted"];

    public function state(){
        return $this->belongsTo(\App\Models\State::class, 'order_state', 'state_id');
    }

    public function good(){
        return $this->belongsTo(\App\Models\Good::class, 'order_good', 'good_id');
    }


//    public function getGoodClientAttribute(){
//        $good = $this->good;
//        $text = $good->good_name . " ". $good->advert->user_first_name . " " .$good->advert->user_last_name . " (" . $good->advert->email . ")";
//        return $text;
//    }

    public function getOrderAddTimeFormattedAttribute(){
        return $this->order_add_time->format("d.m.y H:i");
    }
}