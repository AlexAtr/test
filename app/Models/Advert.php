<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Advert extends Authenticatable
{

    protected $primaryKey = 'user_id';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'user_id', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getInfoTextAttribute(){
        return $this->user_first_name . " ".
                $this->user_last_name . " / " .
                $this->email;
    }
}
