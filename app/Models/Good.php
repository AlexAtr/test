<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Good extends Model
{

    protected $primaryKey = "good_id";

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'good_id', 'created_at', 'updated_at'
    ];



    public function orders(){
        return $this->hasMany(\App\Models\Order::class, 'order_good', 'good_id');
    }

    public function advert(){
        return $this->belongsTo(\App\Models\Advert::class, 'good_advert', 'user_id');
    }
}