<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods', function (Blueprint $table) {
            $table->increments('good_id');
            $table->string('good_name');
            $table->float('good_price');
            $table->integer('good_advert')->unsigned();
            $table->integer('good_external_id')->default(0)->unsigned();
            $table->timestamps();

            $table->foreign('good_advert')->references('user_id')->on('adverts')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('goods');
    }
}
