<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id');
            $table->integer('order_state')->unsigned();
            $table->integer('order_good')->unsigned();
            $table->dateTime('order_add_time');
            $table->string('user_client_phone', 20);
            $table->string('user_client_name');
            $table->timestamps();

            $table->foreign('order_state')->references('state_id')->on('states')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('order_good')->references('good_id')->on('goods')
                ->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
