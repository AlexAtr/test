<?php

use Illuminate\Database\Seeder;

class AdvertsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('adverts')->truncate();

        DB::transaction(function(){

            $faker = Faker\Factory::create();

            \App\Models\Advert::create([
                'user_first_name' => 'Admin',
                'user_last_name' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('admin'),
            ]);

            for($i=0; $i<10; $i++){
                \App\Models\Advert::create([
                    'user_first_name' => $faker->firstName,
                    'user_last_name' => $faker->lastName,
                    'email' => $faker->email,
                    'password' => bcrypt('tester'),
                ]);

            }
        });
    }
}
