<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

         $this->call(AdvertsSeeder::class);
         $this->call(StatesSeeder::class);
         $this->call(GoodsSeeder::class);
         $this->call(OrdersSeeder::class);

        Eloquent::reguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
