<?php

use Illuminate\Database\Seeder;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->truncate();

        DB::transaction(function(){


            \App\Models\State::create([
                'state_name' => "Новый",
                'state_slug' => "new"
            ]);
            \App\Models\State::create([
                'state_name' => "В работе",
                'state_slug' => "onoperator"
            ]);
            \App\Models\State::create([
                'state_name' => "Подтвержден",
                'state_slug' => "accepted"
            ]);



        });
    }
}
