<?php

use Illuminate\Database\Seeder;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->truncate();

        DB::transaction(function(){

            $faker = Faker\Factory::create();

            $goods = \App\Models\Good::all();

            foreach($goods as $i => $good){
                $good->orders()->create([
                    "order_state" => ($i % 3) + 1,
                    "user_client_phone" => $faker->phoneNumber,
                    "user_client_name"  => $faker->name,
                    "order_add_time" => $faker->dateTime
                ]);
            }

        });
    }
}
