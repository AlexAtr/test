<?php

use Illuminate\Database\Seeder;

class GoodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('goods')->truncate();

        DB::transaction(function(){


            \App\Models\Good::create([
                'good_name' => "Часы Rado Integral",
                'good_price' => 2000,
                'good_advert' => 1,
                'good_external_id' => rand(100, 1000)
            ]);
            \App\Models\Good::create([
                'good_name' => "Часы Swiss Army",
                'good_price' => 1500,
                'good_advert' => 1,
                'good_external_id' => rand(100, 1000)
            ]);
            \App\Models\Good::create([
                'good_name' => "Детский планшет",
                'good_price' => 2100,
                'good_advert' => 2,
                'good_external_id' => rand(100, 1000)
            ]);
            \App\Models\Good::create([
                'good_name' => "Колонки Monster Beats",
                'good_price' => 900,
                'good_advert' => 3,
                'good_external_id' => rand(100, 1000)
            ]);


        });
    }
}
