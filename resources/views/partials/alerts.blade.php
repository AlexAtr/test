<div @if(!session('errors')&& !session('success')) style="display:none;" @endif >
         @if(session('errors'))
              @foreach(session('errors')->all() as $msg)
                 <div class="alert alert-danger"> {{$msg}}
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                 </div>
              @endforeach
          @endif
         @if(session('success'))
              @foreach(session('success')->all() as $msg)
                  <div class="alert alert-success">{{$msg}}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              @endforeach
          @endif
</div>
