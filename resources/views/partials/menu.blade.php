
<div class="row">
  <div class="col-sm-12">
    <ul class="nav nav-pills">
        <li role="presentation" @if($_activeMenu == "orders") class="active" @endif><a href="{!! route('orders') !!}"> Заказы </a></li>
        <li role="presentation" @if($_activeMenu == "goods") class="active" @endif><a href="{!! route('goods') !!}"> Товары </a></li>
        <li role="presentation"><a href="{!! route('user_logout') !!}">Выйти</a></li>
    </ul>
  </div>
</div>