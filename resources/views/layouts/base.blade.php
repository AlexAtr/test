<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru-ru">
<head>
    <title>{{$_siteTitle}}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="description" content="{{$_siteDescription}}" />

@section("styles")
    <link href="/plugins/bootstrtap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
        <link rel="stylesheet" href="/css/styles.css">

@show

</head>
<body class="{{$_bodyClass}}" id="{{$_bodyId}}">

@include("partials.alerts")

@yield("content")

@section("scripts")
    <script src="/plugins/jquery.min.js"></script>
    <script src="/plugins/bootstrtap/js/bootstrap.min.js"></script>
    <script src="/plugins/datatables/js/jquery.dataTables.min.js"></script>
@show
</body>
</html>