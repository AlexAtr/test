@extends("layouts.base")

@section("content")

    <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
            {!! Form::open(array('class'=>'form-login', "role"=>"form")) !!}

            <div class="content-login">
                <div class="header">Account Login</div>

                <div class="form-group inputs">
                    {!! Form::email("email", null, ["required" => "required", "class" => "form-control", "placeholder" => "Email"])!!}
                </div>
                <div class="form-group inputs">
                    {!! Form::password("password", ["class" => "form-control", "placeholder" => "Password"])!!}
                </div>
                {{--<div class="link-1">{{link_to_route('login.register', 'Create New Account')}}</div>--}}
                {{--<div class="link-2">{{link_to_route('password.remind', 'Forgot Password?')}}</div>--}}
                <div class="clearfix"></div>
                <div class="button-login">{!! Form::submit('Sign In', ['class' => 'btn btn-primary btn-lg btn-block', 'type' => 'button']) !!} </div>
            </div>

            <div class="footer-login">
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@stop