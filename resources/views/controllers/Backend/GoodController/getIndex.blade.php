@extends("layouts.base")


@section("content")

    <div class="container">
            @include("partials.menu")

        <div class="row">
            <div class="col-xs-12">
                <table id="goods-table" class="display" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th> # </th>
                        <th> Название </th>
                        <th> Цена </th>
                        <th> Рекламодатель </th>
                    </tr>
                    </thead>

                </table>
            </div>
        </div>
    </div>
@stop
@section("scripts")
    @parent
    <script src="/js/controllers/goods.js"></script>
@endsection