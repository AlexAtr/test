@extends("layouts.base")



@section("content")
    <div class="container">
       @include("partials.menu")

        {!! Form::model($good) !!}

        <div class="row">
            <div class="col-xs-3 pull-right">
                {!! Form::submit('Сохранить', ['class' => 'btn btn-success btn-block']) !!}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">
                    <label>ID</label>
                    {!! Form::text("good_external_id", null, ["class" => "form-control"]) !!}
                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group">
                    <label>Рекламодатель</label>
                    {!! Form::select("good_advert", $adverts,  null, ["class" => "form-control"]) !!}
                </div>
             </div>
            <div class="col-sm-5">
                <div class="form-group">
                    <label>Название</label>
                    {!! Form::text("good_name",  null, ["class" => "form-control"]) !!}
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label>Цена</label>
                    {!! Form::text("good_price",  null, ["class" => "form-control"]) !!}
                </div>
            </div>
        </div>

        {!! Form::close() !!}

    </div>
@stop