@extends("layouts.base")


@section("content")

    <div class="container">
        @include("partials.menu")

        <div class="row">
            <div class="col-sm-3 col-md-2">
                <div class="form-group">
                    <label for="date-from">От</label>
                    <input type="text" class="form-control" id="date-from" placeholder="Дата">
                </div>

            </div>
            <div class="col-sm-3 col-md-2">
                <div class="form-group">
                    <label for="date-to">До</label>
                    <input type="text" class="form-control" id="date-to" placeholder="Дата">
                </div>
            </div>
            <div class="col-sm-3 col-md-2">
                <div class="form-group">
                    <label for="oder-state">Статус</label>
                    <input type="text" class="form-control" id="oder-state" placeholder="Статус">
                </div>
            </div>
            <div class="col-sm-3 col-md-2">
                <div class="form-group">
                    <br >
                    <input type="button" role="button" class="btn btn-primary btn-block"  id="filter-submit" value="Показать" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3 col-md-2">
                <div class="form-group">
                    <label for="order-phone">Телефон</label>
                    <input type="text" class="form-control" id="order-phone" >
                </div>

            </div>
            <div class="col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="order-good">Товар</label>
                    <input type="text" class="form-control" id="order-good">
                </div>
            </div>
            <div class="col-sm-3 col-md-2">
                <div class="form-group">
                    <label for="order-id">ID заказа</label>
                    <input type="text" class="form-control" id="order-id">
                </div>
            </div>

        </div>

        <div class="row">
          <div class="col-xs-12">
            <table id="orders-table" class="display" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th> # </th>
                    <th> Дата </th>
                    <th> Клиент </th>
                    <th> Телефон </th>
                    <th> Товар </th>
                    <th> Статус </th>
                </tr>
                </thead>

            </table>
          </div>
        </div>
    </div>
@stop
@section("scripts")
    @parent
    <script src="/js/controllers/orders.js"></script>
@endsection