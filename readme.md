Развёртывание проекта:

* composer install для установки зависимостей
* нужно скопировать .env.example в .env
* в .env нужно указать параметры подключения к бд
* php artisan key:generate
* php artisan migrate --seed

* логин : admin@admin.com
* пароль: admin